<style>

    @import url('https://fonts.googleapis.com/css2?family=Noto+Serif+TC:wght@500&display=swap');
    @import url('https://fonts.googleapis.com/css2?family=Ballet&family=Brygada+1918&display=swap');

    body{
        background:white;
        
        margin:0px;
    }
    .wrap{
        /* background:white; */

        padding:145px 0 0px 0;
    }
    .photo{
        width:100vw;
        height:150vh;

        position:relative;

        overflow:hidden;
    }
    .level_1{
        width:100vw;
        height:720px;
        
        background:url('images/10_material_3_fix_tiny.webp') no-repeat center center;
        background-size:contain;

        position:absolute;
        top:0;
    }
    .level_2{
        width:80vw;
        height:576px;

        background:url('images/10_material_2_tiny.webp') no-repeat center center;
        background-size:contain;

        position:absolute;
        left:0px;
        top:23px;
    }
    .level_3{
        width:100vw;
        height:720px;

        background:url('images/10_material_1_tiny.webp') no-repeat center center;
        background-size:contain;

        position:absolute;
        left:0px;
        top:-97px;
    }
    @media screen and (max-width: 1024px) {
        .photo{
            height:103vh;
        }
        .level_1{
            height:576px;
        }
        .level_2{
            height:460px;
        }
        .level_3{
            height:576px;
        }
    }
    @media screen and (max-width: 768px) {
        .photo{
            height:104vh;
        }
    }
    @media screen and (max-width: 540px) {
        .photo{
            height:102vh;
        }
        .level_1{
            height:284px;
        }
        .level_2{
            height:227px;
        }
        .level_3{
            height:284px;
            top:-17px;
        }
    }
    @media screen and (max-width: 376px) {
        .photo{
            height:103vh;
        }
    }
    @media screen and (max-width: 320px) {
        .photo{
            height:107vh;
        }
    }
    @media screen and (max-width: 300px) {
        .photo{
            height:108vh;
        }
    }

    @font-face {
        font-family: 'cwTeXMing';
        font-style: normal;
        font-weight: 500;
        src: url(//fonts.gstatic.com/ea/cwtexming/v3/cwTeXMing-zhonly.eot);
        src: url(//fonts.gstatic.com/ea/cwtexming/v3/cwTeXMing-zhonly.eot?#iefix) format('embedded-opentype'),
            url(//fonts.gstatic.com/ea/cwtexming/v3/cwTeXMing-zhonly.woff2) format('woff2'),
            url(//fonts.gstatic.com/ea/cwtexming/v3/cwTeXMing-zhonly.woff) format('woff'),
            url(//fonts.gstatic.com/ea/cwtexming/v3/cwTeXMing-zhonly.ttf) format('truetype');
    }

    .text{
        font-family: 'Noto Serif TC', serif;
        color:black;
        font-size:18px;
    }
    .title{
        font-family: 'Brygada 1918', serif;

        position:absolute;
        top:100px;
        /* left:500px; */
        left:50%;
        margin:0 0 0 -99.5px;

        font-size:24px;
    }
    .text1{
        position:absolute;
        top:395px;
        left:36.5%;
        
        margin:0 0 0 -60.5px;
    }
    .text2{
        position:absolute;
        top:655px;
        left:50%;

        margin:0 0 0 -182px;

        transform:rotate(10deg);
    }
    .text3{
        position:absolute;
        top:895px;
        left:50%;
        
        margin:0 0 0 -117px;
    }
    .text4{
        font-family: 'Ballet', cursive;

        position:absolute;
        top:945px;
        left:50%;

        margin:0 0 0 -38px;
    }
    .fade_in{
        display:none;
    }
    .fade_in_late{
        display:none;
    }
    .fade_out{
        display:none;
    }
    .notice{
        position:absolute;
        top:655px;
        left:50%;

        margin:0 0 0 -45px;
    }
    .switch{
        position:absolute;
        bottom:50px;
        left:50%;

        margin:0 0 0 -45px;

        color: black;
        text-decoration:none;
    }
    @media screen and (max-width: 1024px) {
        .text1{
            top:320px;
        }
        .text2{
            top:550px;
        }
        .text3{
            top:745px;
        }
        .text4{
            top:795px;
        }
        .notice{
            top:550px;
        }
        .fade_out{
            display:initial;
        }
    }
    @media screen and (max-width: 768px) {
        .text1{
            top:320px;
        }
        .text2{
            top:530px;
        }
        .text3{
            top:725px;
        }
        .text4{
            top:775px;
        }
        .notice{
            top:530px;
        }
        .fade_out{
            display:initial;
        }
    }
    @media screen and (max-width: 540px) {
        .text1{
            top:160px;
        }
        .text2{
            top:280px;
        }
        .text3{
            top:400px;
        }
        .text4{
            top:450px;
        }
        .notice{
            top:400px;
        }
        .fade_out{
            display:initial;
        }
    }
    @media screen and (max-width: 376px) {
        .text{
            font-size:16px;
        }
        .title{
            font-size:22px;
            margin:0 0 0 -91px;
        }
        .text1{
            top:160px;
            margin:0 0 0 -54px;
        }
        .text2{
            top:270px;
            margin:0 0 0 -161.5px;
        }
        .text3{
            top:380px;
            margin:0 0 0 -104px;
        }
        .text4{
            top:430px;
            margin:0 0 0 -33.5px;
        }
        .notice{
            top:380px;
            margin:0 0 0 -40px;
        }
        .switch{
            margin:0 0 0 -40px;
        }
        .fade_out{
            display:initial;
        }
    }
    @media screen and (max-width: 320px) {
        .text{
            font-size:14px;
        }
        .title{
            font-size:20px;
            margin:0 0 0 -83px;
        }
        .text1{
            top:160px;
            margin:0 0 0 -47px;
        }
        .text2{
            top:265px;
            margin:0 0 0 -141.5px;
        }
        .text3{
            top:365px;
            margin:0 0 0 -91px;
        }
        .text4{
            top:415px;
            margin:0 0 0 -29.5px;
        }
        .notice{
            top:365px;
            margin:0 0 0 -35px;
        }
        .switch{
            margin:0 0 0 -35px;
        }
        .fade_out{
            display:initial;
        }
    }
    @media screen and (max-width: 300px) {
        .text{
            font-size:12px;
        }
        .title{
            font-size:18px;
            margin:0 0 0 -74.5px;
        }
        .text1{
            top:170px;
            margin:0 0 0 -40.5px;
        }
        .text2{
            top:260px;
            margin:0 0 0 -121px;
        }
        .text3{
            top:350px;
            margin:0 0 0 -78px;
        }
        .text4{
            top:400px;
            margin:0 0 0 -25px;
        }
        .notice{
            top:350px;
            margin:0 0 0 -30px;
        }
        .switch{
            margin:0 0 0 -30px;
        }
        .fade_out{
            display:initial;
        }
    }

</style>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shawn's Art space</title>

    <meta name="description" content="超現實主義 - 網頁藝術">
    <meta name="keywords" content="超現實主義,網頁藝術,藝術,視覺,網頁">
    <meta name="author" content="Shawn">

    <meta property="og:url" content="https://montage-303309.df.r.appspot.com"/>
    <!-- <meta property="og:locale" content="zh_TW" /> -->
    <!-- <meta property="og:type" content="website" /> -->
    <meta property="og:title" content="Shawn's art space" />
    <meta property="og:description" content="超現實主義 - 網頁藝術"/>
    <meta property="og:image" content="images/10.jpeg" />
    <!-- <meta property="og:image:alt" content="縮圖說明文字" /> -->
    <!-- <meta property="og:image:type" content="image/png" /> -->
    <!-- <meta property="og:image:width" content="縮圖width" /> -->
    <!-- <meta property="og:image:height" content="縮圖height" /> -->
</head>
<body>
    <div class="wrap">
        <div class="text title">
            Shawn's Art space
        </div>

        <div class="photo">
            <div class="level_1"></div>
            <!-- <img class="level_1" src="10_material_3.png" alt=""> -->
            <div class="text text1 fade_in">主題 &nbsp 夢境淡水</div>

            <div class="level_2"></div>
            <!-- <img class="level_2" src="10_material_2.png" alt=""> -->
            <div class="text text2 fade_in">透過對圖形的切割 &nbsp 縮放 &nbsp 位移 &nbsp 與色調的變化</div>

            <div class="level_3"></div>
            <!-- <img class="level_3" src="10_material_1.png" alt=""> -->
            <div class="text text3 fade_in">營造出超越現實的視覺感受。</div>
            <div class="text text4 fade_in">&nbsp - &nbsp Shawn</div>

            <div class="text notice fade_out">請向下滑動</div>

            <a  class="text switch fade_in_late" href="index">切換為黑色</a>
        </div>
    </div>
</body>
</html>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/81e18089da.js" crossorigin="anonymous"></script>
<script>
    $width = $('.level_1').width();
    $height = $('.level_1').height();
    // console.log($width);
    // console.log($height);

    // 下拉式展開
    // 紀錄效果是否已觸發
    $mark = 0;
    $width = $(window).width();
    console.log($width);
    
    $(window).scroll(function(){
        $position = $(window).scrollTop();
        $width = $(window).width();
        // console.log($position);
        // console.log($mark);

        if($position > 150 && $mark == 0){
            $mark = 1;
            $l2_top = '143px';
            $l3_top = '143px';

            if($width <= 540){
                $l2_top = '80px';
                $l3_top = '93px';
            }

            $('.level_2').stop();
            $('.level_3').stop();
            $('.level_2').animate({top:$l2_top}, 'linear');
            $('.level_3').animate({top:$l3_top}, 'linear');
            
            $('.fade_in').delay('100').fadeIn();
            if($width <= 1024){
                $('.fade_out').delay('100').fadeOut();
            }
            $('.fade_in_late').delay('3000').fadeIn();
        }

        if($position <= 150 && $mark == 1){
            $mark = 0;
            $l2_top = '23px';
            $l3_top = '-97px';

            if($width <= 540){
                $l2_top = '23px';
                $l3_top = '-17';
            }

            $('.level_2').stop();
            $('.level_3').stop();
            $('.level_2').animate({top:$l2_top},'linear');
            $('.level_3').animate({top:$l3_top},'linear');

            $('.fade_in').fadeOut();
            if($width <= 1024){
                $('.fade_out').fadeIn();
            }
            $('.fade_in_late').stop();
            $('.fade_in_late').fadeOut();
        }

    });

</script>
